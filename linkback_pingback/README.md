# linkback_pingback
Linkback handler module to implement pingback ref-backs.
origin from: https://github.com/aleixq/vinculum-d8

## Installation

Also requires xmlrpc module: https://drupal.org/project/xmlrpc
